#ifndef MEDIAN_STREAM_UTILS_HPP
#define MEDIAN_STREAM_UTILS_HPP

#include <cstddef>
#include <random>
#include <vector>

namespace MedianStream
{

template <typename RNG>
void random_vector(RNG &rng, std::vector<int> &vec,
                   size_t count, int min, int max)
{
    std::uniform_int_distribution<int> uni(min, max);

    vec.resize(count);

    for (int& i : vec)
        i = uni(rng);
}

}

#endif //MEDIAN_STREAM_UTILS_HPP
