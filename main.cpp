#include <iostream>
#include "MedianHeap.hpp"
#include "StreamParser.hpp"

int main()
{
    MedianHeap median;
    return MedianStream::streamParse(std::cin, std::cout, median);
}
