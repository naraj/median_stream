#ifndef MEDIAN_STREAM_MEDIANNAIVE_H
#define MEDIAN_STREAM_MEDIANNAIVE_H

#include <vector>

// For testing
class MedianNaive
{
    float median{};
    std::vector<int> vec;
public:

    MedianNaive() = default;
    explicit MedianNaive(const std::vector<int>& args);
    void insert(int x);
    float getMedian() const;
private:
    float calculateMedian() const;
};

#endif //MEDIAN_STREAM_MEDIANNAIVE_H
