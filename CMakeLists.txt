cmake_minimum_required(VERSION 3.10)
project(median_stream)

set(CMAKE_CXX_STANDARD 11)

set(SRCS
        MedianNaive.hpp
        MedianNaive.cpp
        StreamParser.hpp
        Utils.hpp
        IntVector.hpp
        IntVector.cpp
        Heap.hpp
        Heap.cpp
        MedianHeap.hpp
        MedianHeap.cpp
)

set(TEST_SRCS
        test.cpp
        testMedian.cpp
        testParser.cpp
        testIntVector.cpp
        testHeap.cpp
)

add_executable(median_stream main.cpp ${SRCS})
add_executable(median_stream_test catch.hpp ${TEST_SRCS} ${SRCS})

enable_testing()
add_test(NAME median_stream_test COMMAND median_stream_test)
