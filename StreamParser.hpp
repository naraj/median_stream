#ifndef MEDIAN_STREAM_STREAM_PARSER_HPP
#define MEDIAN_STREAM_STREAM_PARSER_HPP

#include <cctype>
#include <iostream>
#include <iomanip>

namespace MedianStream
{

static inline bool isNotInt(char c)
{
    return !(isdigit(c) || (c == '-'));
}

bool stringIsValidInt(const std::string &str)
{
    for (char c : str)
    {
        if (isNotInt(c))
            return false;
    }
    return true;
}

template <typename MedianT>
int streamParse(std::istream &input, std::ostream &output, MedianT &median)
{
    bool quit = false;
    std::string s;

    output << std::fixed << std::setprecision(1);

    while (!quit)
    {
        input >> s;

        if (s == "m")
        {
            output << median.getMedian() << " ";
        }
        else if (s == "q")
        {
            quit = true;
        }
        else if (stringIsValidInt(s))
        {
            median.insert(std::stoi(s));
        }
        else
        {
            return -1;
        }
    }

    return 0;
}

}

#endif //MEDIAN_STREAM_STREAM_PARSER_HPP
