#ifndef MEDIAN_STREAM_HEAP_HPP
#define MEDIAN_STREAM_HEAP_HPP

#include "IntVector.hpp"

class Heap
{
    IntVector vec;
    bool (*comparator)(int, int);
public:
    static bool greater(int a, int b);
    static bool smaller(int a, int b);

    explicit Heap(bool (*comparator)(int, int) = smaller);

    bool empty() const;
    size_t size() const;

    int top() const;
    void push(int x);
    void pop();
private:
    void heapify_down(size_t i);
    void heapify_up(size_t i);
    size_t parent(size_t i) const;
    size_t left(size_t i) const;
    size_t right(size_t i) const;
};


#endif //MEDIAN_STREAM_HEAP_HPP
