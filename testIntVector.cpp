#include "catch.hpp"

#include "IntVector.hpp"

TEST_CASE("Test vector resizing", "[vector]")
{
    IntVector v;

    REQUIRE(v.empty());
    REQUIRE(v.size() == 0);
    REQUIRE(v.capacity() == 0);
    REQUIRE(v.ptr() == nullptr);

    v.push_back(0);

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 1);
    REQUIRE(v.capacity() == 1);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);

    v.push_back(1);

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 2);
    REQUIRE(v.capacity() == 2);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);

    v.push_back(2);

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 3);
    REQUIRE(v.capacity() == 4);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);
    REQUIRE(v.at(2) == 2);

    v.push_back(3);

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 4);
    REQUIRE(v.capacity() == 4);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);
    REQUIRE(v.at(2) == 2);
    REQUIRE(v.at(3) == 3);

    v.push_back(4);

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 5);
    REQUIRE(v.capacity() == 8);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);
    REQUIRE(v.at(2) == 2);
    REQUIRE(v.at(3) == 3);
    REQUIRE(v.at(4) == 4);

    v.pop_back();

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 4);
    REQUIRE(v.capacity() == 8);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);
    REQUIRE(v.at(2) == 2);
    REQUIRE(v.at(3) == 3);

    v.pop_back();

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 3);
    REQUIRE(v.capacity() == 8);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);
    REQUIRE(v.at(2) == 2);

    v.pop_back();

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 2);
    REQUIRE(v.capacity() == 8);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);
    REQUIRE(v.at(1) == 1);

    v.pop_back();

    REQUIRE(!v.empty());
    REQUIRE(v.size() == 1);
    REQUIRE(v.capacity() == 8);
    REQUIRE(v.ptr() != nullptr);
    REQUIRE(v.at(0) == 0);

    v.pop_back();

    REQUIRE(v.empty());
    REQUIRE(v.size() == 0);
    REQUIRE(v.capacity() == 8);
    REQUIRE(v.ptr() != nullptr);
}
