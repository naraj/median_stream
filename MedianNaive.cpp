#include <algorithm>
#include <vector>

#include "MedianNaive.hpp"

MedianNaive::MedianNaive(const std::vector<int>& args) : median(0.0)
{
    for (int n : args)
        vec.push_back(n);

    std::sort(std::begin(vec), std::end(vec));
    median = calculateMedian();
}

void MedianNaive::insert(int x)
{
    vec.push_back(x);
    std::sort(std::begin(vec), std::end(vec));
    median = calculateMedian();
}

float MedianNaive::getMedian() const
{
    return median;
}

float MedianNaive::calculateMedian() const
{
    size_t size = vec.size();

    if (size == 0)
        return 0.0;

    if (size % 2 == 0)
        return (vec[size / 2 - 1] + vec[size / 2]) / 2.0f;

    return vec[size / 2];
}
