#ifndef MEDIAN_STREAM_VECTOR_HPP
#define MEDIAN_STREAM_VECTOR_HPP

#include <cstddef>

class IntVector
{
    int *m_ptr;
    size_t m_size, m_capacity;
public:
    IntVector();
    ~IntVector();

    bool empty() const;
    size_t size() const;
    size_t capacity() const;
    void push_back(int value );
    void pop_back();
    int *ptr() const;

    int &at(size_t index);
    int at(size_t index) const;

private:
    void realloc(size_t new_size);
};


#endif //MEDIAN_STREAM_VECTOR_HPP
