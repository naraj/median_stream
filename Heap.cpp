#include <stdexcept>
#include "Heap.hpp"

void swap(int &a, int &b)
{
    int aux = a;
    a = b;
    b = aux;
}

bool Heap::greater(int a, int b)
{
    return a > b;
}

bool Heap::smaller(int a, int b)
{
    return a < b;
}

Heap::Heap(bool (*comparator)(int, int)) : comparator(comparator)
{ }

bool Heap::empty() const
{
    return size() == 0;
}

size_t Heap::size() const
{
    return vec.size();
}

int Heap::top() const
{
    if (size() == 0)
        throw std::out_of_range("The heap is empty");

    return vec.at(0);
}

void Heap::push(int x)
{
    vec.push_back(x);
    heapify_up(size() - 1);
}

void Heap::pop()
{
    if (size() == 0)
        throw std::out_of_range("The heap is empty");

    swap(vec.at(0), vec.at(size() - 1));
    vec.pop_back();
    heapify_down(0);
}

// Iterative version of the heapify_down algorithm.
// Start with the root and go to leaves
void Heap::heapify_down(size_t i)
{
    bool changed = true;

    while(changed)
    {
        // Get left and right child of node at index i
        size_t left = Heap::left(i);
        size_t right = Heap::right(i);

        // Subroot is a root of a current subtree
        size_t subroot = i;

        // Compare subroot with its left and right child
        // and find the new subroot value
        if (left < size() && comparator(vec.at(left), vec.at(subroot)))
            subroot = left;

        if (right < size() && comparator(vec.at(right), vec.at(subroot)))
            subroot = right;

        // Stop the loop if the subroot didn't change
        // There is no need for heapifying further
        if (subroot == i)
        {
            changed = false;
        }
        else
        {
            swap(vec.at(i), vec.at(subroot));
            i = subroot;
        }
    }
}

// Iterative version of the heapify_up algorithm.
// Start with a leaf and work your way up to the root.
void Heap::heapify_up(size_t i)
{
    // if current node violates the condition against it's parent,
    // swap and move to the parent
    while (i && comparator(vec.at(i), vec.at(parent(i))))
    {
        swap(vec.at(i), vec.at(parent(i)));
        i = parent(i);
    }
}

size_t Heap::parent(size_t i) const
{
    return (i - 1) / 2;
}

size_t Heap::left(size_t i) const
{
    return (2 * i + 1);
}

size_t Heap::right(size_t i) const
{
    return (2 * i + 2);
}
