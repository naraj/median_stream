#include "catch.hpp"

#include "Heap.hpp"

TEST_CASE("Test heap push/pop/empty/size operations", "[heap]")
{
    Heap h;

    REQUIRE(h.empty());
    REQUIRE(h.size() == 0);

    h.push(1);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 1);
    REQUIRE(h.top() == 1);

    h.push(2);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 2);
    REQUIRE(h.top() == 1);

    h.push(3);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 3);
    REQUIRE(h.top() == 1);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 2);
    REQUIRE(h.top() == 2);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 1);
    REQUIRE(h.top() == 3);

    h.pop();
    REQUIRE(h.empty());
    REQUIRE(h.size() == 0);
}

TEST_CASE("Test min heap", "[heap]")
{
    Heap h;

    REQUIRE(h.empty());
    REQUIRE(h.size() == 0);

    h.push(5);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 1);
    REQUIRE(h.top() == 5);

    h.push(6);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 2);
    REQUIRE(h.top() == 5);

    h.push(3);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 3);
    REQUIRE(h.top() == 3);

    h.push(4);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 4);
    REQUIRE(h.top() == 3);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 3);
    REQUIRE(h.top() == 4);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 2);
    REQUIRE(h.top() == 5);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 1);
    REQUIRE(h.top() == 6);
}

TEST_CASE("Test max heap", "[heap]")
{
    Heap h(Heap::greater);

    REQUIRE(h.empty());
    REQUIRE(h.size() == 0);

    h.push(10);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 1);
    REQUIRE(h.top() == 10);

    h.push(1);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 2);
    REQUIRE(h.top() == 10);

    h.push(3);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 3);
    REQUIRE(h.top() == 10);

    h.push(5);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 4);
    REQUIRE(h.top() == 10);

    h.push(20);
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 5);
    REQUIRE(h.top() == 20);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 4);
    REQUIRE(h.top() == 10);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 3);
    REQUIRE(h.top() == 5);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 2);
    REQUIRE(h.top() == 3);

    h.pop();
    REQUIRE(!h.empty());
    REQUIRE(h.size() == 1);
    REQUIRE(h.top() == 1);
}
