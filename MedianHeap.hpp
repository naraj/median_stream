#ifndef MEDIAN_STREAM_MEDIANHEAP_H
#define MEDIAN_STREAM_MEDIANHEAP_H

#include "Heap.hpp"

#include <vector>

class MedianHeap
{
    float median{};
    Heap lower;
    Heap higher;

public:
    MedianHeap();
    // For testing
    explicit MedianHeap(const std::vector<int>& args);
    void insert(int x);
    float getMedian() const;
};

#endif //MEDIAN_STREAM_MEDIANHEAP_H
