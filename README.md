# Median in a stream of integers

This project contains an implementation of a solution to the problem of finding 
a median element in a stream of integers in an efficient way. 

The chosen algorithm is based on the use of two heap data structures, that hold 
the numbers being received by the program. There is a max heap that
holds the numbers that are lower than the effective median and a min heap that 
holds those that are higher than the effective median.

When a new number arrives it is added either to the max heap or to the min heap
depending on how it compares to the current effective median. After that, the 
number of elements in heaps differs by at most one element. When
both heaps contain the same number of elements, the effective median is calculated 
as an average of the tops of the two heaps. When the number of elements in one 
heap is larger the effective median is the top of that heap. 

#### Complexity analysis

To be more specific, the data structure used in this algorithm is called
a binary heap. It is similar to a binary tree, but it has two additional constraints:
- all levels of the tree except possibly the last one are fully filled
and the last level is filled from left to right
- the key stored in each node is either greater or less than (depending on the type
of the heap) the key in the node's children.

When adding or removing an element from a heap, the elements are added/removed
from the end of the heap. This is to conform to the shape property but the after 
this operation the heap property no longer holds. Then it is restored by traversing 
up or down the heap. Because the height of the binary heap is log n, 
the add/remove operation takes O(log n) time.

This operation is performed at most three times when adding a new element 
(when re-balancing min and max heaps) so this does not change 
the time complexity.

The complexity of finding a median after adding/removing an element is O(1), as
we only need to return the top of the larger heap or return an average of 
the tops of two heaps.

So the algorithm performs n insertions at O(log n) cost each, which makes
the time complexity of finding the median in a stream of integers O(n log n).

### Application

One of the goals was to create a console application that would read a stream of 
data through the standard input and print the output to standard output.

#### Input:

The application was designed to handle input in the form of a single row of data 
that can contain an arbitrary number of integers and letters ('m' and 'q') separated 
by spaces. The integers denote elements added to a set. The letter 'm' denotes 
a request to calculate and return a median of elements inserted so far.
The letter 'q' denotes the end of the stream.

#### Output:

A sequence of floating-point numbers for each median request ('m') in the 
input stream, separated by single spaces.

#### Example:

| Input           | Output      |
|-----------------|-------------|
| 3 5 m 8 m 6 m q | 4.0 5.0 5.5 |

### Prerequisites

This project requires a compiler supporting C++11 standard and CMake version
3.10 at least. It should support older version of CMake but it wasn't tested
with them.

### Building

To build the project simply do:

```
mkdir build
cd build
cmake ..
make
```

## Running the application

Inside build directory there should be an executable `median_stream`. When you
execute the application it waits for the user input.

```
> ./median_stream
3 5 m 8 m 6 m q
4.0 5.0 5.5
```

## Running the tests

The tests were written with [Catch2 unit-testing library](https://github.com/catchorg/Catch2).
The library was attached to the project in a header-only version.
To run the tests go into your build directory and run:

```
make test
```

Example:

```
> make test
Running tests...
Test project /home/michaln/Workspace_priv/median_stream/build
    Start 1: median_stream_test
1/1 Test #1: median_stream_test ...............   Passed    1.47 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   1.47 sec
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
