#include "MedianHeap.hpp"

MedianHeap::MedianHeap() : median(0.0),
                           lower(Heap::greater),
                           higher()
{ }

MedianHeap::MedianHeap(const std::vector<int>& args) : median(0.0),
                                                       lower(Heap::greater),
                                                       higher()
{
    for (int n : args)
        insert(n);
}

void MedianHeap::insert(int x)
{
    if (lower.empty())
    {
        lower.push(x);
        median = x;
        return;
    }

    if (lower.size() > higher.size())
    {
        if (x < median)
        {
            higher.push(lower.top());
            lower.pop();
            lower.push(x);
        }
        else
        {
            higher.push(x);
        }

        // Now both heaps have the same size
        // Median is an average of the tops of the heaps
        median = (lower.top() + higher.top()) / 2.f;
    }
    else if (lower.size() < higher.size())
    {
        if (x > median)
        {
            lower.push(higher.top());
            higher.pop();
            higher.push(x);
        }
        else
        {
            lower.push(x);
        }

        // Now both heaps have the same size
        // Median is an average of the tops of the heaps
        median = (lower.top() + higher.top()) / 2.f;
    }
    else
    {
        // Median is the top of the heap with higher
        // number of elements after adding the current one
        if (x < median)
        {
            lower.push(x);
            median = lower.top();
        }
        else
        {
            higher.push(x);
            median = higher.top();
        }
    }

}

float MedianHeap::getMedian() const
{
    return median;
}
