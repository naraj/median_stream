#include <sstream>
#include <iterator>
#include "catch.hpp"

#include "MedianNaive.hpp"
#include "MedianHeap.hpp"
#include "StreamParser.hpp"
#include "Utils.hpp"

using MedianType = MedianHeap;

using namespace MedianStream;

TEST_CASE("Parser Test invalid string 1", "[parser]")
{
    int ret;
    MedianType median;
    std::istringstream input;
    std::ostringstream output;

    input.str("10 23 stuff");

    ret = streamParse(input, output, median);

    REQUIRE(ret == -1);
    REQUIRE(output.str().empty());
}

TEST_CASE("Parser Test invalid string 2", "[parser]")
{
    int ret;
    MedianType median;
    std::istringstream input;
    std::ostringstream output;

    input.str("11sddasd123");

    ret = streamParse(input, output, median);

    REQUIRE(ret == -1);
    REQUIRE(output.str().empty());
}

TEST_CASE("Parser Example 1", "[parser]")
{
    int ret;
    MedianType median;
    std::istringstream input;
    std::ostringstream output;

    input.str("3 5 m 8 m 6 m q");

    ret = streamParse(input, output, median);

    REQUIRE(ret == 0);
    REQUIRE(output.str() == "4.0 5.0 5.5 ");
}

TEST_CASE("Parser Example 2", "[parser]")
{
    int ret;
    MedianType median;
    std::istringstream input;
    std::ostringstream output;

    input.str("m 5 m 10 m 15 m q");

    ret = streamParse(input, output, median);

    REQUIRE(ret == 0);
    REQUIRE(output.str() == "0.0 5.0 7.5 10.0 ");
}

TEST_CASE("Parser Example 3", "[parser]")
{
    int ret;
    MedianType median;
    std::istringstream input;
    std::ostringstream output;

    input.str("m -1 m 1 m -1 m q");

    ret = streamParse(input, output, median);

    REQUIRE(ret == 0);
    REQUIRE(output.str() == "0.0 -1.0 0.0 -1.0 ");
}

TEST_CASE("Parser Example 4", "[parser]")
{
    int ret;
    MedianType median;
    std::istringstream input;
    std::ostringstream output;

    input.str("m -10 m -20 m 10 m q");

    ret = streamParse(input, output, median);

    REQUIRE(ret == 0);
    REQUIRE(output.str() == "0.0 -10.0 -15.0 -10.0 ");
}

TEST_CASE("Parser random list", "[parser]")
{
    std::mt19937 rng(0);
    std::vector<int> vec;
    std::istringstream inputNaive;
    std::istringstream inputHeap;
    std::ostringstream outputNaive;
    std::ostringstream outputHeap;
    std::stringstream result;
    MedianNaive medianNaive;
    MedianType medianHeap;

    SECTION("10 random from 0 to 100")
    {
        random_vector(rng, vec, 10, 0, 100);
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<int>(result, " "));
        result << " m q";
        inputNaive.str(result.str());
        inputHeap.str(result.str());

        REQUIRE(streamParse(inputNaive, outputNaive, medianNaive) == 0);
        REQUIRE(streamParse(inputHeap, outputHeap, medianHeap) == 0);
        REQUIRE(outputNaive.str() == outputHeap.str());
    }

    SECTION("10 random from -100 to 100")
    {
        random_vector(rng, vec, 10, -100, 100);
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<int>(result, " "));
        result << " m q";
        inputNaive.str(result.str());
        inputHeap.str(result.str());

        REQUIRE(streamParse(inputNaive, outputNaive, medianNaive) == 0);
        REQUIRE(streamParse(inputHeap, outputHeap, medianHeap) == 0);
        REQUIRE(outputNaive.str() == outputHeap.str());
    }

    SECTION("1000 random from -100 to 100")
    {
        random_vector(rng, vec, 1000, -100, 100);
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<int>(result, " "));
        result << " m q";
        inputNaive.str(result.str());
        inputHeap.str(result.str());

        REQUIRE(streamParse(inputNaive, outputNaive, medianNaive) == 0);
        REQUIRE(streamParse(inputHeap, outputHeap, medianHeap) == 0);
        REQUIRE(outputNaive.str() == outputHeap.str());
    }

    SECTION("1000 random from INT_MIN / 2 to INT_MAX / 2")
    {
        random_vector(rng, vec, 1000, std::numeric_limits<int>::min() / 2,
                      std::numeric_limits<int>::max() / 2);
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<int>(result, " "));
        result << " m q";
        inputNaive.str(result.str());
        inputHeap.str(result.str());

        REQUIRE(streamParse(inputNaive, outputNaive, medianNaive) == 0);
        REQUIRE(streamParse(inputHeap, outputHeap, medianHeap) == 0);
        REQUIRE(outputNaive.str() == outputHeap.str());
    }

    SECTION("1000 random from INT_MIN to INT_MAX")
    {
        random_vector(rng, vec, 1000, std::numeric_limits<int>::min(),
                      std::numeric_limits<int>::max());
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<int>(result, " "));
        result << " m q";
        inputNaive.str(result.str());
        inputHeap.str(result.str());

        REQUIRE(streamParse(inputNaive, outputNaive, medianNaive) == 0);
        REQUIRE(streamParse(inputHeap, outputHeap, medianHeap) == 0);
        REQUIRE(outputNaive.str() == outputHeap.str());
    }
}
