#include <random>

#include "catch.hpp"

#include "MedianNaive.hpp"
#include "MedianHeap.hpp"
#include "Utils.hpp"

using MedianType = MedianHeap;

using namespace MedianStream;

TEST_CASE("Median Example 1", "[median]")
{
    MedianType median;

    REQUIRE(median.getMedian() == 0.0);

    median.insert(3);

    median.insert(5);

    REQUIRE(median.getMedian() == 4.0);

    median.insert(8);

    REQUIRE(median.getMedian() == 5.0);

    median.insert(6);

    REQUIRE(median.getMedian() == 5.5);
}

TEST_CASE("Median Example 2", "[median]")
{
    MedianType median;

    REQUIRE(median.getMedian() == 0.0);

    median.insert(5);

    REQUIRE(median.getMedian() == 5.0);

    median.insert(10);

    REQUIRE(median.getMedian() == 7.5);

    median.insert(15);

    REQUIRE(median.getMedian() == 10.0);
}

TEST_CASE("Median Example 3", "[median]")
{
    MedianType median;

    REQUIRE(median.getMedian() == 0.0);

    median.insert(-1);

    REQUIRE(median.getMedian() == -1.0);

    median.insert(1);

    REQUIRE(median.getMedian() == 0);

    median.insert(-1);

    REQUIRE(median.getMedian() == -1.0);
}

TEST_CASE("Median Example 4", "[median]")
{
    MedianType median;

    REQUIRE(median.getMedian() == 0.0);

    median.insert(-10);

    REQUIRE(median.getMedian() == -10.0);

    median.insert(-20);

    REQUIRE(median.getMedian() == -15);

    median.insert(10);

    REQUIRE(median.getMedian() == -10.0);
}

TEST_CASE("Median random list", "[median]")
{
    std::mt19937 rng(0);
    std::vector<int> vec;

    SECTION("10 random from 0 to 100")
    {
        random_vector(rng, vec, 10, 0, 100);
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    SECTION("10 random from -100 to 100")
    {
        random_vector(rng, vec, 10, -100, 100);
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    SECTION("1000 random from -100 to 100")
    {
        random_vector(rng, vec, 1000, -100, 100);
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    SECTION("10000 random from -100 to 100")
    {
        random_vector(rng, vec, 10000, -100, 100);
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    SECTION("10000 random from INT_MIN / 2 to INT_MAX / 2")
    {
        random_vector(rng, vec, 10000, std::numeric_limits<int>::min() / 2,
                      std::numeric_limits<int>::max() / 2);
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    // getting an average from two INT_MAX would result in integer overflow
    // but the two version of the algorithm should still give the same answer
    SECTION("10000 random from INT_MIN to INT_MAX")
    {
        random_vector(rng, vec, 10000, std::numeric_limits<int>::min(),
                      std::numeric_limits<int>::max());
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    SECTION("1000000 random from INT_MIN/2 to INT_MAX/2")
    {
        random_vector(rng, vec, 1000000, std::numeric_limits<int>::min() / 2,
                      std::numeric_limits<int>::max() / 2);
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }

    // getting an average from two INT_MAX would result in integer overflow
    // but the two version of the algorithm should still give the same answer
    SECTION("10000 random from INT_MAX to INT_MAX")
    {
        random_vector(rng, vec, 10000, std::numeric_limits<int>::max(),
                      std::numeric_limits<int>::max());
        MedianNaive medianNaive(vec);
        MedianType medianHeap(vec);

        REQUIRE(medianNaive.getMedian() == medianHeap.getMedian());
    }
}
