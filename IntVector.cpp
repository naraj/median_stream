#include <cstring>
#include <stdexcept>
#include "IntVector.hpp"

IntVector::IntVector() : m_ptr(nullptr), m_size(0), m_capacity(0)
{ }

IntVector::~IntVector()
{
    delete[] m_ptr;
}

bool IntVector::empty() const
{
    return m_size == 0;
}

size_t IntVector::size() const
{
    return m_size;
}

size_t IntVector::capacity() const
{
    return m_capacity;
}

int *IntVector::ptr() const
{
    return m_ptr;
}

int &IntVector::at(size_t index)
{
    if (index >= m_size)
        throw std::out_of_range("Index out of range");

    return m_ptr[index];
}

int IntVector::at(size_t index) const
{
    if (index >= m_size)
        throw std::out_of_range("Index out of range");

    return m_ptr[index];
}

void IntVector::push_back(int value)
{
    if (m_capacity == 0)
    {
        m_capacity = 1;
        m_ptr = new int[m_capacity];
    }
    else if (m_size >= m_capacity)
    {
        m_capacity *= 2;
        realloc(m_capacity);
    }

    m_ptr[m_size] = value;
    ++m_size;
}

void IntVector::pop_back()
{
    if (m_size == 0)
        throw std::out_of_range("Index out of range");

    --m_size;
}

void IntVector::realloc(size_t new_size)
{
    int *tmp = new int[new_size];
    std::memcpy(tmp, m_ptr, m_size * sizeof(int));
    delete[] m_ptr;
    m_ptr = tmp;
}
